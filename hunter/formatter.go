package hunter

import "time"

type HunterFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Email string `json:"email"`
	Active bool `json:"active"`
	Confirmation bool `json:"confirmation"`
	CreatedAt time.Time
}

func FormatHunter(hunter Hunter) HunterFormat {
	fHunter := HunterFormat{
		ID:           hunter.ID,
		Name:         hunter.Name,
		Email:        hunter.Name,
		Active:       false,
		Confirmation: false,
		CreatedAt:    hunter.CreatedAt,
	}
	if hunter.Active == 1 {
		fHunter.Active = true
	}
	if hunter.Confirmation == 1 {
		fHunter.Confirmation = true
	}
	return fHunter
}