-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE schedule_ustads (
     id            serial NOT NULL PRIMARY KEY,
     ustad_id      bigint NOT NULL,
     day_id        int NOT NULL,
     time_start    TIME NOT NULL,
     time_end      TIME NOT NULL,
     created_at    TIMESTAMP NULL DEFAULT NULL,
     updated_at    TIMESTAMP NULL DEFAULT NULL
);
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE schedule_ustads;