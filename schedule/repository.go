package schedule

import "gorm.io/gorm"

type Repository interface {
	SaveOrUpdate(schedule Schedule) (Schedule, error)
	SaveBatch(schedules []Schedule) ([]Schedule, error)
	CheckExist(schedule Schedule) (bool, error)
	FindByHunterID(hunterID int) ([]Schedule, error)
	FindByUstadID(ustadID int) ([]Schedule, error)
	FindByID(ID int) (Schedule, error)
	Cancel(schedule Schedule) (Schedule, error)
	FindByIDWithUstad(ID int) (Schedule, error)
	FindByIDWithHunter(ID int) (Schedule, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) SaveOrUpdate(schedule Schedule) (Schedule, error){
	err := r.db.Save(&schedule).Error
	if err != nil {
		return schedule, err
	}
	return schedule, nil
}

func (r *repository) SaveBatch(schedules []Schedule) ([]Schedule, error){
	err := r.db.Save(&schedules).Error
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (r *repository) CheckExist(schedule Schedule) (bool, error){
	err := r.db.Where("ustad_id = ? and hunter_id = ? and date = ? and time_start = ? and time_end = ? and confirmation = 0 and is_done = 0 and is_canceled = 0",
		schedule.UstadID, schedule.HunterID, schedule.Date, schedule.TimeStart, schedule.TimeEnd).Find(&schedule).Error
	if err != nil {
		return false, err
	}
	if schedule.ID != 0 {
		return true, nil
	}
	return false, nil
}

func (r *repository) FindByHunterID(hunterID int) ([]Schedule, error) {
	schedules := []Schedule{}
	err := r.db.Preload("CancelSchedule").Preload("Ustad").Where("hunter_id = ?", hunterID).Find(&schedules).Error
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (r *repository) FindByUstadID(ustadID int) ([]Schedule, error) {
	schedules := []Schedule{}
	err := r.db.Preload("CancelSchedule").Preload("Hunter").Where("ustad_id = ?", ustadID).Find(&schedules).Error
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (r *repository) Cancel(schedule Schedule) (Schedule, error) {
	err := r.db.Preload("CancelSchedule").Save(&schedule).Error
	if err != nil {
		return schedule, err
	}
	return schedule, nil
}

func (r *repository) FindByID(ID int) (Schedule, error) {
	var schedule Schedule
	err := r.db.Where("id = ?", ID).Find(&schedule).Error
	if err != nil {
		return schedule, err
	}
	return schedule,nil
}

func (r *repository) FindByIDWithUstad(ID int) (Schedule, error) {
	var schedule Schedule
	err := r.db.Preload("CancelSchedule").Preload("Ustad").Where("id = ?", ID).Find(&schedule).Error
	if err != nil {
		return schedule, err
	}
	return schedule, nil
}

func (r *repository) FindByIDWithHunter(ID int) (Schedule, error) {
	var schedule Schedule
	err := r.db.Preload("CancelSchedule").Preload("Hunter").Where("id = ?", ID).Find(&schedule).Error
	if err != nil {
		return schedule, err
	}
	return schedule, nil
}