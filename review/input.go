package review

type CreateReviewInput struct {
	ScheduleID int `json:"schedule_id" binding:"required"`
	HunterID int `json:"hunter_id" binding:"required"`
	UstadID int `json:"ustad_id" binding:"required"`
	Star int `json:"star" binding:"required"`
	Description string `json:"description" binding:"required"`
}

type UstadReviewInput struct {
	UstadID int `form:"ustad_id" binding:"required"`
}
