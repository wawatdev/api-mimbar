package ustad

type CreateUstadInput struct {
	Name string
	Email string
	Password string
}

type CreateScheduleUstadInput struct {
	UstadID int
	DayID []int `json:"day_id" binding:"required"`
	TimeStart string `json:"time_start" binding:"required"`
	TimeEnd string `json:"time_end" binding:"required"`
}

type UpdateUstadInput struct {
	ID int
	Name string `json:"name" binding:"required"`
	Password string `json:"password"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}