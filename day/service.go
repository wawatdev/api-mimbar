package day

type Service interface {
	GetAll() ([]Day, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) GetAll() ([]Day, error){
	days, err := s.repository.FindAll()
	if err != nil {
		return days, err
	}
	return days, nil
}
