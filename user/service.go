package user

import (
	"api-mimbar/hunter"
	"api-mimbar/ustad"
	"errors"
	"golang.org/x/crypto/bcrypt"
)

type Service interface {
	Login(input LoginInput) (User, error)
	Register(input RegisterInput) (User, error)
	GetUserByIDAndRole(userID int, role string) (User, error)
	UpdateProfile(input UpdateProfileInput) (User, error)
}

type service struct {
	hunterService hunter.Service
	ustadService ustad.Service
}

func NewService(hunterService hunter.Service, ustadService ustad.Service) *service {
	return &service{hunterService, ustadService}
}

func (s *service) Login(input LoginInput) (User, error){
	//find ustad
	var user User
	ustadData, err := s.ustadService.GetByEmail(input.Email)
	if err != nil {
		return user, err
	}
	if ustadData.ID == 0 {
		//find hunter
		hunterData, err := s.hunterService.GetByEmail(input.Email)
		if err != nil {
			return user,err
		}
		if hunterData.ID == 0 {
			return user, errors.New("user not found")
		}

		if hunterData.Active == 0 {
			return user, errors.New("user not actived")
		}

		err = bcrypt.CompareHashAndPassword([]byte(hunterData.Password), []byte(input.Password))
		if err != nil {
			return user, err
		}
		user = mappingHunterToUser(hunterData)
		return user, nil
	}

	if ustadData.Active == 0 {
		return user, errors.New("user not actived")
	}

	if ustadData.Confirmation == 0 {
		return user, errors.New("user not yet confirm")
	}

	err = bcrypt.CompareHashAndPassword([]byte(ustadData.Password), []byte(input.Password))
	if err != nil {
		return user, err
	}
	user = mappingUstadToUser(ustadData)
	return user, nil
}

func (s *service) Register(input RegisterInput) (User, error){
	var user User
	
	registerBy := ""
	if input.Role == "ustad" {
		registerBy = "ustad"
	}else if input.Role == "hunter" {
		registerBy = "hunter"
	}

	if registerBy == "ustad" {
		ustadData, err := s.ustadService.GetByEmail(input.Email)
		if err != nil {
			return user, err
		}
		if ustadData.ID != 0 {
			return user, errors.New("email already exist")
		}
		inputData := ustad.CreateUstadInput{
			Name:     input.Name,
			Email:    input.Email,
			Password: input.Password,
		}
		ustadData, err = s.ustadService.Create(inputData)
		if err != nil {
			return user, err
		}
		user = mappingUstadToUser(ustadData)
		return user, nil
	}else if registerBy == "hunter" {
		hunterData, err := s.hunterService.GetByEmail(input.Email)
		if err != nil {
			return user, err
		}
		if hunterData.ID != 0 {
			return user, errors.New("email already exist")
		}
		inputData := hunter.CreateHunterInput{
			Name:     input.Name,
			Email:    input.Email,
			Password: input.Password,
		}
		hunterData, err = s.hunterService.Create(inputData)
		if err != nil {
			return user, err
		}
		user = mappingHunterToUser(hunterData)
		return user, nil
	}
	return user, errors.New("can't process")
}

func (s *service) GetUserByIDAndRole(userID int, role string) (User, error){
	var user User
	if role == "ustad" {
		ustad, err := s.ustadService.GetByID(userID)
		if err != nil {
			return user, err
		}

		user = mappingUstadToUser(ustad)
		if ustad.Active != 1 {
			return user, errors.New("user not active")
		}

		if ustad.Confirmation != 1 {
			return user, errors.New("user need confirmation")
		}
		return user, nil
	}else if role == "hunter" {
		hunter, err := s.hunterService.GetByID(userID)
		if err != nil {
			return user, err
		}

		user = mappingHunterToUser(hunter)

		if hunter.Active != 1 {
			return user, errors.New("user not actived")
		}
		return user, nil
	}
	return user, errors.New("user not found")
}

func (s *service) UpdateProfile(input UpdateProfileInput) (User, error) {
	var user User
	if input.Role == "ustad" {
		ustadInput := ustad.UpdateUstadInput{
			ID:       input.UserID,
			Name:     input.Name,
			Password: input.Password,
		}
		ustadData, err := s.ustadService.Update(ustadInput)
		if err != nil {
			return user, err
		}
		user = mappingUstadToUser(ustadData)
		return user, nil
	}else if input.Role == "hunter" {
		hunterInput := hunter.UpdateHunterInput{
			ID:       input.UserID,
			Name:     input.Name,
			Password: input.Password,
		}
		hunterData, err := s.hunterService.Update(hunterInput)
		if err != nil {
			return user, err
		}
		user = mappingHunterToUser(hunterData)
		return user, nil
	}
	return user, errors.New("data can't be process")
}

func mappingUstadToUser(ustad ustad.Ustad) User {
	user := User{
		ID:           ustad.ID,
		Name:         ustad.Name,
		Email:        ustad.Email,
		Active:       false,
		Confirmation: false,
		Status:       ustad.Status,
		Role:         "ustad",
		Phone: ustad.Phone,
		CreatedAt:    ustad.CreatedAt,
		UpdatedAt:    ustad.UpdatedAt,
	}
	if ustad.Active == 1 {
		user.Active = true
	}
	if ustad.Confirmation == 1 {
		user.Confirmation = true
	}
	return user
}

func mappingHunterToUser(hunter hunter.Hunter) User {
	user := User{
		ID:           hunter.ID,
		Name:         hunter.Name,
		Email:        hunter.Email,
		Active:       false,
		Confirmation: false,
		Status:       "",
		Role:         "hunter",
		Phone: hunter.Phone,
		CreatedAt:    hunter.CreatedAt,
		UpdatedAt:    hunter.UpdatedAt,
	}
	if hunter.Active == 1 {
		user.Active = true
	}
	if hunter.Confirmation == 1 {
		user.Confirmation = true
	}
	return user
}