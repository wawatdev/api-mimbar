package handler

import (
	"api-mimbar/day"
	"api-mimbar/helper"
	"github.com/gin-gonic/gin"
	"net/http"
)

type dayHandler struct {
	service day.Service
}

func NewDayHandler(service day.Service) *dayHandler {
	return &dayHandler{service}
}

func (h *dayHandler) GetAll(c *gin.Context) {
	days, err := h.service.GetAll()
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed get days", http.StatusUnprocessableEntity,errMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Get days successfully", http.StatusOK, day.FormatDays(days))
	c.JSON(http.StatusOK, response)
	return
}
