package review

import (
	"api-mimbar/hunter"
	"time"
)

type Review struct {
	ID int
	ScheduleID int
	UstadID int
	HunterID int
	Star int
	Description string
	CreatedAt time.Time
	UpdatedAt time.Time
	Hunter hunter.Hunter
}
