package handler

import (
	"api-mimbar/helper"
	"api-mimbar/schedule"
	"api-mimbar/user"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type scheduleHandler struct {
	service schedule.Service
}

func NewScheduleHandler(service schedule.Service) *scheduleHandler {
	return &scheduleHandler{service}
}

func (h *scheduleHandler) Create(c *gin.Context) {
	var input schedule.CreateScheduleInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errMessage := gin.H{"error":helper.FormatValidationError(err)}
		response := helper.ApiResponse("Create Schedule failed", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	input.HunterID = c.MustGet("currentUser").(user.User).ID
	scheduleDatas, err := h.service.Create(input)
	if err != nil {
		response := helper.ApiResponse("Create Schedule failed", http.StatusUnprocessableEntity, err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Success Create schedule", http.StatusOK, schedule.FormatCreateUpdateSchedules(scheduleDatas))
	c.JSON(http.StatusOK, response)
	return
}

func (h *scheduleHandler) GetAll(c *gin.Context) {
	userLogin := c.MustGet("currentUser").(user.User)
	role := userLogin.Role
	if role == "hunter" {
		schedules, err := h.service.GetAllByHunterID(userLogin.ID)
		if err != nil {
			response := helper.ApiResponse("Failed get schedule", http.StatusUnprocessableEntity, err.Error())
			c.JSON(http.StatusUnprocessableEntity, response)
			return
		}
		response := helper.ApiResponse("List of Schedules", http.StatusOK, schedule.SchedulesHunterFormat(schedules))
		c.JSON(http.StatusOK, response)
		return
	}else if role == "ustad" {
		schedules, err := h.service.GetAllByUstadID(userLogin.ID)
		if err != nil {
			response := helper.ApiResponse("Failed get schedule", http.StatusUnprocessableEntity, err.Error())
			c.JSON(http.StatusUnprocessableEntity, response)
			return
		}
		response := helper.ApiResponse("List of Schedules", http.StatusOK, schedule.SchedulesUstadFormat(schedules))
		c.JSON(http.StatusOK, response)
		return
	}else{
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "data can't be process"})
		return
	}
}

func (h *scheduleHandler) CancelSchedule(c *gin.Context) {
	var input schedule.CancelScheduleInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response := helper.ApiResponse("failed cancel schedule", http.StatusBadRequest, err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	userData := c.MustGet("currentUser").(user.User)
	userRole := userData.Role
	newSchedule, err := h.service.CancelSchedule(input, userRole)
	if err != nil {
		response := helper.ApiResponse("failed cancel schedule", http.StatusBadRequest, err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Cancel schedule successfully", http.StatusOK, schedule.FormatCancelSchedule(newSchedule))
	c.JSON(http.StatusOK, response)
	return
}

func (h *scheduleHandler) Confirmation(c *gin.Context) {
	var input schedule.ConfirmationScheduleInput
	err := c.ShouldBindQuery(&input)
	if err != nil {
		errMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("failed confirm schedule", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	scheduleID, _ := strconv.Atoi(c.Query("schedule_id"))
	userLogin := c.MustGet("currentUser").(user.User)

	scheduleData, err := h.service.Confirmation(scheduleID, userLogin)
	if err != nil {
		response := helper.ApiResponse("failed confirmation schedule", http.StatusUnprocessableEntity, err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Confirmation schedule successfully", http.StatusOK, schedule.FormatCreateUpdateSchedule(scheduleData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *scheduleHandler) GetByID(c *gin.Context) {
	var input schedule.GetScheduleInput
	err := c.ShouldBindUri(&input)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed get schedule", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	user := c.MustGet("currentUser").(user.User)
	userRole := user.Role

	scheduleData, err := h.service.GetByIDWithRole(input.ID, userRole)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed get schedule", http.StatusUnprocessableEntity, errMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}



	if userRole == "hunter" {
		if scheduleData.HunterID != user.ID {
			errMessage := gin.H{"error":"you don't have authorized with this schedule"}
			response := helper.ApiResponse("failed get schedule", http.StatusUnprocessableEntity, errMessage)
			c.JSON(http.StatusUnprocessableEntity, response)
			return
		}

		response := helper.ApiResponse("Get Schedule Successfully", http.StatusOK, schedule.ScheduleHunterFormat(scheduleData))
		c.JSON(http.StatusOK, response)
		return
	}else if userRole == "ustad" {
		if scheduleData.UstadID != user.ID {
			errMessage := gin.H{"error":"you don't have authorized with this schedule"}
			response := helper.ApiResponse("failed get schedule", http.StatusUnprocessableEntity, errMessage)
			c.JSON(http.StatusUnprocessableEntity, response)
			return
		}

		response := helper.ApiResponse("Get Schedule Successfully", http.StatusOK, schedule.ScheduleUstadFormat(scheduleData))
		c.JSON(http.StatusOK, response)
		return
	}else{
		errMessage := gin.H{"error":"data can't be process"}
		response := helper.ApiResponse("failed get schedule", http.StatusUnprocessableEntity, errMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
}

func (h *scheduleHandler) Done(c *gin.Context) {
	var input schedule.ConfirmationScheduleInput
	err := c.ShouldBindQuery(&input)
	if err != nil {
		errMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("failed confirm schedule", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	ScheduleID, _ := strconv.Atoi(c.Query("schedule_id"))
	userLogin := c.MustGet("currentUser").(user.User)

	scheduleData, err := h.service.Done(ScheduleID, userLogin)
	if err != nil {
		response := helper.ApiResponse("failed done schedule", http.StatusUnprocessableEntity, err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("schedule done successfully", http.StatusOK, schedule.FormatSchedule(scheduleData))
	c.JSON(http.StatusOK, response)
	return
}