package auth

import (
	"api-mimbar/config"
	"errors"
	"github.com/dgrijalva/jwt-go"
)

type Service interface {
	GenerateToken(userID int, role string) (string, error)
	ValidateToken(token string) (*jwt.Token, error)
}

type jwtService struct {

}

func NewService() *jwtService {
	return &jwtService{}
}

func (s *jwtService) GenerateToken(userID int, role string) (string, error){
	claim := jwt.MapClaims{}
	claim["user_id"] = userID
	claim["role"] = role

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	signedToken, err := token.SignedString([]byte(config.Secret_key))
	if err != nil {
		return signedToken, err
	}
	return signedToken, nil
}

func (s *jwtService) ValidateToken(token string) (*jwt.Token, error){
	newToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New("invalid token")
		}
		return []byte(config.Secret_key), nil
	})

	if err != nil {
		return newToken, err
	}
	return newToken, nil
}
