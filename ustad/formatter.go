package ustad

import "time"

type UstadFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Email string `json:"email"`
	Active bool `json:"active"`
	Confirmation bool `json:"confirmation"`
	Status string `json:"status"`
	CreatedAt time.Time `json:"created_at"`
	Schedules []ScheduleUstadFormat `json:"schedules"`
}

func FormatUstad(ustad Ustad) UstadFormat {
	fUstad := UstadFormat{
		ID:           ustad.ID,
		Name:         ustad.Name,
		Email:        ustad.Email,
		Active:       false,
		Confirmation: false,
		Status:       ustad.Status,
		CreatedAt: ustad.CreatedAt,
		Schedules:    FormatSchedulesUstad(ustad.ScheduleUstad),
	}
	if ustad.Active == 1 {
		fUstad.Active = true
	}
	if ustad.Confirmation == 1 {
		fUstad.Confirmation = true
	}
	return fUstad
}

func FormatUstads(ustads []Ustad) []UstadFormat {
	fUstads := []UstadFormat{}

	if len(ustads) > 0 {
		for _, ustad := range ustads {
			fUstad := FormatUstad(ustad)
			fUstads = append(fUstads, fUstad)
		}
	}
	return fUstads
}

type ScheduleUstadFormat struct {
	ID int `json:"id"`
	UstadID int `json:"ustad_id"`
	DayID int `json:"day_id"`
	DayName string `json:"day_name"`
	TimeStart string `json:"time_start"`
	TimeEnd string `json:"time_end"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func FormatScheduleUstad(schedule ScheduleUstad) ScheduleUstadFormat {
	return ScheduleUstadFormat{
		ID:        schedule.ID,
		UstadID:   schedule.UstadID,
		DayID:     schedule.DayID,
		DayName:   schedule.Day.Name,
		TimeStart: schedule.TimeStart,
		TimeEnd:   schedule.TimeEnd,
		CreatedAt: schedule.CreatedAt,
		UpdatedAt: schedule.UpdatedAt,
	}
}

func FormatSchedulesUstad(schedules []ScheduleUstad) []ScheduleUstadFormat {
	fSchedules := []ScheduleUstadFormat{}
	if len(schedules) > 0 {
		for _, schedule := range schedules {
			fSchedule := FormatScheduleUstad(schedule)
			fSchedules = append(fSchedules, fSchedule)
		}
	}
	return fSchedules
}
