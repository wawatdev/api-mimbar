-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE days (
      id          serial NOT NULL PRIMARY KEY,
      name    varchar (25) NOT NULL,
      created_at  TIMESTAMP NULL DEFAULT NULL,
      updated_at  TIMESTAMP NULL DEFAULT NULL
);

INSERT INTO days (name, created_at, updated_at) VALUES ('Senin','2021-01-17 16:26:34','2021-01-17 16:26:34');
INSERT INTO days (name, created_at, updated_at) VALUES ('Selasa','2021-01-17 16:26:34','2021-01-17 16:26:34');
INSERT INTO days (name, created_at, updated_at) VALUES ('Rabu','2021-01-17 16:26:34','2021-01-17 16:26:34');
INSERT INTO days (name, created_at, updated_at) VALUES ('Kamis','2021-01-17 16:26:34','2021-01-17 16:26:34');
INSERT INTO days (name, created_at, updated_at) VALUES ('Jumat','2021-01-17 16:26:34','2021-01-17 16:26:34');
INSERT INTO days (name, created_at, updated_at) VALUES ('Sabtu','2021-01-17 16:26:34','2021-01-17 16:26:34');
INSERT INTO days (name, created_at, updated_at) VALUES ('Minggu','2021-01-17 16:26:34','2021-01-17 16:26:34');
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE days;