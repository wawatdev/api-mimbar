package review

import (
	"api-mimbar/schedule"
	"errors"
	"time"
)

type Service interface {
	Create(input CreateReviewInput) (Review, error)
	GetByUstadID(ustadID int) ([]Review, error)
}

type service struct {
	repository Repository
	scheduleService schedule.Service
}

func NewService(repository Repository, scheduleService schedule.Service) *service {
	return &service{repository, scheduleService}
}

func (s *service) Create(input CreateReviewInput) (Review, error) {
	reviewData := Review{
		ScheduleID:  input.ScheduleID,
		UstadID:     input.UstadID,
		HunterID:    input.HunterID,
		Star:        input.Star,
		Description: input.Description,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	if input.Star > 5 {
		return reviewData, errors.New("stars can't more than 5")
	}else if input.Star < 1{
		return reviewData, errors.New("stars can't less than 1")
	}
	scheduleData, err := s.scheduleService.GetByID(input.ScheduleID)
	if err != nil {
		return reviewData, err
	}
	if scheduleData.IsCanceled == 1 {
		return reviewData, errors.New("schedule has been cancel before")
	}
	if scheduleData.Confirmation != 1 {
		return reviewData, errors.New("need confirmation before")
	}
	if scheduleData.IsDone != 1 {
		return reviewData, errors.New("schedule not yet done")
	}
	if scheduleData.UstadID != input.UstadID || scheduleData.HunterID != input.HunterID{
		return reviewData, errors.New("you don't have permission with this schedule")
	}

	checkAvailable, err := s.repository.CheckAvailable(reviewData)
	if err != nil {
		return reviewData, err
	}
	if checkAvailable {
		return reviewData, errors.New("review has been created before")
	}

	reviewData, err = s.repository.SaveOrUpdate(reviewData)
	if err != nil {
		return reviewData, err
	}
	return reviewData, nil
}

func (s *service) GetByUstadID(ustadID int) ([]Review, error) {
	reviews, err := s.repository.FindByUstadID(ustadID)
	if err != nil {
		return reviews, err
	}
	return reviews, nil
}
