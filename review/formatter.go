package review

import (
	"time"
)

type ReviewFormat struct {
	ID int `json:"id"`
	ScheduleID int `json:"schedule_id"`
	HunterID int `json:"hunter_id"`
	UstadID int `json:"ustad_id"`
	Star int `json:"star"`
	Description string `json:"description"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func FormatReview(review Review) ReviewFormat {
	return ReviewFormat{
		ID: 		 review.ID,
		ScheduleID:  review.ScheduleID,
		HunterID:    review.HunterID,
		UstadID:     review.UstadID,
		Star:        review.Star,
		Description: review.Description,
		CreatedAt:   review.CreatedAt,
		UpdatedAt:   review.UpdatedAt,
	}
}

type UstadReviewFormat struct {
	ReviewFormat
	Hunter HunterFormat
}

type HunterFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Email string `json:"email"`
	Active bool `json:"active"`
	Confirmation bool `json:"confirmation"`
	CreatedAt time.Time
}

func FormatUstadReview(review Review) UstadReviewFormat {
	fReview := UstadReviewFormat{
		ReviewFormat: FormatReview(review),
		Hunter:       HunterFormat{
			ID:           review.HunterID,
			Name:         review.Hunter.Name,
			Email:        review.Hunter.Email,
			Active:       false,
			Confirmation: false,
			CreatedAt:    review.Hunter.CreatedAt,
		},
	}
	if review.Hunter.Active == 1 {
		fReview.Hunter.Active = true
	}
	if review.Hunter.Confirmation == 1 {
		fReview.Hunter.Confirmation = true
	}
	return fReview
}

func FormatUstadReviews(reviews []Review) []UstadReviewFormat {
	fReviews := []UstadReviewFormat{}
	if len(reviews) > 0 {
		for _, review := range reviews {
			fReview := FormatUstadReview(review)
			fReviews = append(fReviews, fReview)
		}
	}
	return fReviews
}
