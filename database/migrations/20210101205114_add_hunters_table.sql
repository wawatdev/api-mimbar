-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE hunters (
   id          serial NOT NULL PRIMARY KEY,
   name        varchar(250) NOT NULL,
   email       varchar(255) NOT NULL UNIQUE,
   password    varchar(255) NOT NULL,
   phone       varchar(30) NULL UNIQUE DEFAULT NULL,
   active      int NOT NULL,
   confirmation int NOT NULL,
   email_verified TIMESTAMP NULL DEFAULT NULL,
   created_at  TIMESTAMP NULL DEFAULT NULL,
   updated_at  TIMESTAMP NULL DEFAULT NULL
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE hunters;