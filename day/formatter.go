package day

import "time"

type DayFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func FormatDay(day Day) DayFormat {
	return DayFormat{
		ID:        day.ID,
		Name:      day.Name,
		CreatedAt: day.CreatedAt,
		UpdatedAt: day.UpdatedAt,
	}
}

func FormatDays(days []Day) []DayFormat {
	fDays := []DayFormat{}
	if len(days) > 0 {
		for _, day := range days {
			fDay := FormatDay(day)
			fDays = append(fDays, fDay)
		}
	}
	return fDays
}
