package hunter

import "gorm.io/gorm"

type Repository interface {
	SaveOrUpdate(hunter Hunter) (Hunter, error)
	FindByEmail(email string) (Hunter, error)
	FindByID(ID int) (Hunter, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB)	*repository {
	return &repository{db}
}

func (r *repository) SaveOrUpdate(hunter Hunter) (Hunter, error){
	err := r.db.Save(&hunter).Error
	if err != nil {
		return hunter, err
	}
	return hunter, nil
}

func (r *repository) FindByEmail(email string) (Hunter, error){
	var hunter Hunter
	err := r.db.Where("email = ?", email).Find(&hunter).Error
	if err != nil {
		return hunter, err
	}
	return hunter, nil
}

func (r *repository) FindByID(ID int) (Hunter, error){
	var hunter Hunter
	err := r.db.Where("id = ?", ID).Find(&hunter).Error
	if err != nil {
		return hunter, err
	}
	return hunter, nil
}
