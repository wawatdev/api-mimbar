package main

import (
	"api-mimbar/auth"
	"api-mimbar/config"
	"api-mimbar/day"
	"api-mimbar/handler"
	"api-mimbar/helper"
	"api-mimbar/hunter"
	"api-mimbar/review"
	"api-mimbar/schedule"
	"api-mimbar/user"
	"api-mimbar/ustad"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"net/http"
	"strings"
)

func main() {

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable TimeZone=Asia/Jakarta",
		config.PGSQL_HOST,config.PGSQL_USER,config.PGSQL_PASSWORD,config.PGSQL_DB,config.PGSQL_PORT)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err.Error())
	}

	dayRepository := day.NewRepository(db)
	dayService := day.NewService(dayRepository)
	dayHandler := handler.NewDayHandler(dayService)

	hunterRepository := hunter.NewRepository(db)
	hunterService := hunter.NewService(hunterRepository)
	hunterHandler := handler.NewHunterHandler(hunterService)

	ustadRepository := ustad.NewRepository(db)
	ustadService := ustad.NewService(ustadRepository)
	ustadHandler := handler.NewUstadHandler(ustadService)

	scheduleRepository := schedule.NewRepository(db)
	scheduleService := schedule.NewService(scheduleRepository, ustadService)
	scheduleHandler := handler.NewScheduleHandler(scheduleService)

	reviewRepository := review.NewRepository(db)
	reviewService := review.NewService(reviewRepository, scheduleService)
	reviewHandler := handler.NewReviewHandler(reviewService)

	userService := user.NewService(hunterService, ustadService)
	authService := auth.NewService()
	userHandler := handler.NewUserHandler(userService, authService)
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, "Golang server is running..")
		return
	})
	api := router.Group("api/v1")

	//api admin
	apiAdmin := api.Group("/admin")
	apiAdmin.POST("/hunters", hunterHandler.Create)
	apiAdmin.POST("/ustads", ustadHandler.Create)

	//api front end
	api.POST("/reviews", authMiddleware(authService, userService, []string{"hunter"}), reviewHandler.Create)
	api.PUT("/schedules/done", authMiddleware(authService, userService, []string{"hunter","ustad"}), scheduleHandler.Done)
	api.PUT("/schedules/confirmation", authMiddleware(authService, userService, []string{"ustad"}), scheduleHandler.Confirmation)
	api.PUT("/schedules/cancel", authMiddleware(authService, userService, []string{"hunter"}), scheduleHandler.CancelSchedule)
	api.GET("/schedules", authMiddleware(authService, userService, []string{"hunter","ustad"}), scheduleHandler.GetAll)
	api.GET("/schedules/:id", authMiddleware(authService, userService, []string{"hunter","ustad"}), scheduleHandler.GetByID)
	api.POST("/schedules", authMiddleware(authService, userService, []string{"hunter"}), scheduleHandler.Create)
	api.GET("/ustads", authMiddleware(authService, userService, []string{"hunter","ustad"}), ustadHandler.GetAllWithActiveAndConfirm)
	api.POST("/ustads/schedules", authMiddleware(authService, userService, []string{"ustad"}), ustadHandler.CreateSchedule)
	api.PUT("/ustads/schedules", authMiddleware(authService, userService, []string{"ustad"}), ustadHandler.UpdateSchedule)
	api.GET("/ustads/reviews", authMiddleware(authService, userService, []string{"ustad","hunter"}), reviewHandler.GetByUstadID)

	api.PUT("/users/update-profile", authMiddleware(authService, userService, []string{"ustad","hunter"}), userHandler.UpdateProfile)

	api.GET("days", dayHandler.GetAll)
	api.POST("/login", userHandler.Login)
	api.POST("/register", userHandler.Register)

	router.Run()
}

func authMiddleware(authService auth.Service, userService user.Service, roles []string) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if !strings.Contains(authHeader, "Bearer") {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		tokenString := ""
		arrayToken := strings.Split(authHeader, " ")
		if len(arrayToken) == 2 {
			tokenString = arrayToken[1]
		}

		token, err := authService.ValidateToken(tokenString)
		if err != nil {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		userID := int(claim["user_id"].(float64))
		userRole := claim["role"].(string)

		validateRole := false

		if len(roles) > 0 {
			for _, role := range roles {
				if role == userRole {
					validateRole = true
				}
			}
		}
		if validateRole == false{
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		user, err := userService.GetUserByIDAndRole(userID, userRole)
		if err != nil {
			response := helper.ApiResponse("Unauthorized", http.StatusUnprocessableEntity, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}
		c.Set("currentUser",user)
	}
}
