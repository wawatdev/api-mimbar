package user


type LoginInput struct {
	Email string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

type RegisterInput struct {
	Name string `json:"name" binding:"required"`
	Email string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
	Role string `json:"role" binding:"required"`
}

type UpdateProfileInput struct {
	Name string `json:"name" binding:"required"`
	Password string `json:"password"`
	UserID int
	Role string
}