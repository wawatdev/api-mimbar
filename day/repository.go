package day

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]Day, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]Day, error) {
	var days []Day
	err := r.db.Find(&days).Error
	if err != nil {
		return days, err
	}
	return days, nil
}
