package handler

import (
	"api-mimbar/helper"
	"api-mimbar/review"
	"github.com/gin-gonic/gin"
	"net/http"
)

type reviewHandler struct {
	service review.Service
}

func NewReviewHandler(service review.Service) *reviewHandler {
	return &reviewHandler{service}
}

func (h *reviewHandler) Create(c *gin.Context) {
	var input review.CreateReviewInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed create review", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	reviewData, err := h.service.Create(input)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed create review", http.StatusUnprocessableEntity, errMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("create review successfully", http.StatusOK, review.FormatReview(reviewData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *reviewHandler) GetByUstadID(c *gin.Context) {
	var input review.UstadReviewInput
	err := c.ShouldBindQuery(&input)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed get reviews", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	reviews, err := h.service.GetByUstadID(input.UstadID)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed get reviews", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List ustad Reviews", http.StatusOK, review.FormatUstadReviews(reviews))
	c.JSON(http.StatusOK, response)
	return
}
