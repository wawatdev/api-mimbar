package handler

import (
	"api-mimbar/auth"
	"api-mimbar/helper"
	"api-mimbar/user"
	"github.com/gin-gonic/gin"
	"net/http"
)

type userHandler struct {
	service user.Service
	authService auth.Service
}

func NewUserHandler(service user.Service, authService auth.Service) *userHandler {
	return &userHandler{service, authService}
}

func (h *userHandler) Login(c *gin.Context) {
	var input user.LoginInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response := helper.ApiResponse("Login failed", http.StatusBadRequest, gin.H{"error":err.Error()})
		c.JSON(http.StatusBadRequest, response)
		return
	}

	userData, err := h.service.Login(input)
	if err != nil {
		response := helper.ApiResponse("Login failed", http.StatusUnprocessableEntity, gin.H{"error":err.Error()})
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	token, err := h.authService.GenerateToken(userData.ID, userData.Role)
	if err != nil {
		response := helper.ApiResponse("Login failed", http.StatusUnprocessableEntity, gin.H{"error":err.Error()})
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("login Successfully", http.StatusOK, user.FormatLogin(userData, token))
	c.JSON(http.StatusOK, response)
	return
}

func (h *userHandler) Register(c *gin.Context) {
	var input user.RegisterInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response := helper.ApiResponse("Register failed", http.StatusBadRequest, gin.H{"error":err.Error()})
		c.JSON(http.StatusBadRequest, response)
		return
	}

	userData, err := h.service.Register(input)
	if err != nil {
		response := helper.ApiResponse("Register failed", http.StatusUnprocessableEntity, gin.H{"error":err.Error()})
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Register Successfully", http.StatusOK, user.FormatLogin(userData, ""))
	c.JSON(http.StatusOK, response)
	return
}

func (h *userHandler) UpdateProfile(c *gin.Context) {
	var input user.UpdateProfileInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response := helper.ApiResponse("update profile failed", http.StatusBadRequest, gin.H{"error":err.Error()})
		c.JSON(http.StatusBadRequest, response)
		return
	}

	userLogin := c.MustGet("currentUser").(user.User)
	input.UserID = userLogin.ID
	input.Role = userLogin.Role
	userData, err := h.service.UpdateProfile(input)
	if err != nil {
		response := helper.ApiResponse("update profile failed", http.StatusUnprocessableEntity, gin.H{"error":err.Error()})
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("update profile successfully", http.StatusOK, user.FormatUser(userData))
	c.JSON(http.StatusOK, response)
	return
}
