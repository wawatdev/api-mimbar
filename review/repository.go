package review

import (
	"gorm.io/gorm"
)

type Repository interface {
	SaveOrUpdate(review Review) (Review, error)
	CheckAvailable(review Review) (bool, error)
	FindByUstadID(ustadID int) ([]Review, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) SaveOrUpdate(review Review) (Review, error){
	err := r.db.Save(&review).Error
	if err != nil {
		return review, err
	}
	return review, nil
}

func (r *repository) CheckAvailable(review Review) (bool, error){
	err := r.db.Where("schedule_id = ? and ustad_id = ? and hunter_id = ?", review.ScheduleID, review.UstadID, review.HunterID).Find(&review).Error
	if err != nil {
		return false, err
	}
	if review.ID != 0 {
		return true, nil
	}
	return false, nil
}

func (r *repository) FindByUstadID(ustadID int) ([]Review, error){
	var reviews []Review
	err := r.db.Preload("Hunter").Where("ustad_id = ?", ustadID).Order("id desc").Find(&reviews).Error
	if err != nil {
		return reviews, err
	}
	return reviews, nil
}
