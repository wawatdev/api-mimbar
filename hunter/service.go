package hunter

import (
	"errors"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type Service interface {
	Create(input CreateHunterInput) (Hunter, error)
	Update(input UpdateHunterInput) (Hunter, error)
	GetByEmail(email string) (Hunter, error)
	GetByID(ID int) (Hunter, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) Create(input CreateHunterInput) (Hunter, error){
	hunter := Hunter{
		Name:         input.Name,
		Email:        input.Email,
		Password:     "",
		Active:       1,
		Confirmation: 0,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
	if err != nil {
		return hunter, err
	}

	hunter.Password = string(passwordHash)

	hunter, err = s.repository.SaveOrUpdate(hunter)
	if err != nil {
		return hunter, err
	}
	return hunter, nil
}

func (s *service) GetByEmail(email string) (Hunter, error){
	hunter, err := s.repository.FindByEmail(email)
	if err != nil {
		return hunter, err
	}
	return hunter, nil
}

func (s *service) GetByID(ID int) (Hunter, error){
	hunter, err := s.repository.FindByID(ID)
	if err != nil {
		return hunter, err
	}
	if hunter.ID == 0 {
		return hunter, errors.New("user not found")
	}
	return hunter, nil
}

func (s *service) Update(input UpdateHunterInput) (Hunter, error) {
	hunterData, err := s.GetByID(input.ID)
	if err != nil {
		return hunterData, err
	}

	hunterData.Name = input.Name
	if input.Email != "" {
		hunterData.Email = input.Email
	}
	if input.Password != "" {
		passwordHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
		if err != nil {
			return hunterData, err
		}
		hunterData.Password = string(passwordHash)
	}

	hunterData, err = s.repository.SaveOrUpdate(hunterData)
	if err != nil {
		return hunterData, err
	}
	return hunterData, nil
}
