package ustad

import (
	"errors"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type Service interface {
	Create(input CreateUstadInput) (Ustad, error)
	Update(input UpdateUstadInput) (Ustad, error)
	GetByEmail(email string) (Ustad, error)
	GetByID(ID int) (Ustad, error)
	GetByIDActiveAndConfirmed(ID int) (Ustad, error)
	GetAllWithActiveConfirmAndSchedule() ([]Ustad, error)
	CreateSchedule(input CreateScheduleUstadInput) ([]ScheduleUstad, error)
	GetScheduleByUstadID(ustadID int) ([]ScheduleUstad, error)
	UpdateSchedule(input CreateScheduleUstadInput) ([]ScheduleUstad, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) Create(input CreateUstadInput) (Ustad, error){
	ustad := Ustad{
		Name:         input.Name,
		Email:        input.Email,
		Password:     "",
		Active:       1,
		Confirmation: 0,
		Status:       "available",
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
	if err != nil {
		return ustad, err
	}
	ustad.Password = string(passwordHash)

	ustadData, err := s.repository.SaveOrUpdate(ustad)
	if err != nil {
		return ustadData, err
	}
	return ustadData, nil
}

func (s *service) GetByEmail(email string) (Ustad, error){
	ustad, err := s.repository.FindByEmail(email)
	if err != nil {
		return ustad, err
	}
	return ustad, nil
}

func (s *service) GetByID(ID int) (Ustad, error){
	ustad, err := s.repository.FindByID(ID)
	if err != nil {
		return ustad, err
	}
	if ustad.ID == 0 {
		return ustad, errors.New("ustad not found")
	}
	return ustad, nil
}

func (s *service) GetByIDActiveAndConfirmed(ID int) (Ustad, error){
	ustad, err := s.repository.FindByID(ID)
	if err != nil {
		return ustad, err
	}
	if ustad.ID == 0 {
		return ustad, errors.New("ustad not found")
	}
	if ustad.Active == 0 {
		return ustad, errors.New("ustad not actived")
	}
	if ustad.Confirmation == 0 {
		return ustad, errors.New("ustad not confirmed")
	}
	return ustad, nil
}

func (s *service) GetAllWithActiveConfirmAndSchedule() ([]Ustad, error) {
	ustads, err := s.repository.FindAllWithActiveConfirmAndSchedule()
	if err != nil {
		return ustads, err
	}
	return ustads, nil
}

func (s *service) CreateSchedule(input CreateScheduleUstadInput) ([]ScheduleUstad, error) {
	var schedules []ScheduleUstad
	if len(input.DayID) < 1 {
		return schedules, errors.New("data can't be process")
	}

	ustadSchedule, err := s.GetScheduleByUstadID(input.UstadID)
	if err != nil {
		return schedules, err
	}

	if len(ustadSchedule) > 0 {
		return schedules, errors.New("schedule has been created")
	}

	for _, day_id := range input.DayID {
		schedule := ScheduleUstad{
			UstadID:   input.UstadID,
			DayID:     day_id,
			TimeStart: input.TimeStart,
			TimeEnd:   input.TimeEnd,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		}
		schedules = append(schedules, schedule)
	}

	schedules, err = s.repository.SaveScheduleBatch(schedules)
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (s *service) GetScheduleByUstadID(ustadID int) ([]ScheduleUstad, error) {
	schedules, err := s.repository.FindScheduleByUstadID(ustadID)
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (s *service) UpdateSchedule(input CreateScheduleUstadInput) ([]ScheduleUstad, error) {
	var schedules []ScheduleUstad
	if len(input.DayID) < 1 {
		return schedules, errors.New("data can't be process")
	}

	ustadSchedule, err := s.GetScheduleByUstadID(input.UstadID)
	if err != nil {
		return schedules, err
	}

	if len(ustadSchedule) > 0 {
		//remove schedule ustad before
		_, err = s.repository.DeleteScheduleByUstadID(input.UstadID)
		if err != nil {
			return schedules, err
		}
	}

	for _, day_id := range input.DayID {
		schedule := ScheduleUstad{
			UstadID:   input.UstadID,
			DayID:     day_id,
			TimeStart: input.TimeStart,
			TimeEnd:   input.TimeEnd,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		}
		schedules = append(schedules, schedule)
	}

	schedules, err = s.repository.SaveScheduleBatch(schedules)
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (s *service) Update(input UpdateUstadInput) (Ustad, error) {
	ustadData, err := s.GetByID(input.ID)
	if err != nil {
		return ustadData, err
	}
	ustadData.Name = input.Name

	if input.Email != "" {
		ustadData.Email = input.Email
	}

	if input.Password != "" {
		passwordHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
		if err != nil {
			return ustadData, err
		}
		ustadData.Password = string(passwordHash)
	}
	ustadData, err = s.repository.SaveOrUpdate(ustadData)
	if err != nil {
		return ustadData, err
	}
	return ustadData, nil
}