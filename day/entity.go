package day

import "time"

type Day struct {
	ID int
	Name string
	CreatedAt time.Time
	UpdatedAt time.Time
}
