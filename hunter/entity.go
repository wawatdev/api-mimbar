package hunter

import "time"

type Hunter struct {
	ID int
	Name string
	Email string
	Password string
	Phone string
	Active int8
	Confirmation int8
	CreatedAt time.Time
	UpdatedAt time.Time
}