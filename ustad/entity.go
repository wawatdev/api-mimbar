package ustad

import (
	"api-mimbar/day"
	"time"
)

type Ustad struct {
	ID int
	Name string
	Email string
	Password string
	Phone string
	Active int8
	Confirmation int8
	Status string
	CreatedAt time.Time
	UpdatedAt time.Time
	ScheduleUstad []ScheduleUstad
}

type ScheduleUstad struct {
	ID int
	UstadID int
	DayID int
	TimeStart string
	TimeEnd string
	CreatedAt time.Time
	UpdatedAt time.Time
	Day day.Day
}