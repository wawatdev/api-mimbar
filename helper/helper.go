package helper

type Meta struct {
	Message string `json:"message"`
	StatusCode int `json:"status_code"`
}

type ResponseBody struct {
	Meta Meta `json:"meta"`
	Data interface{} `json:"data"`
}

func ApiResponse(message string, statusCode int, data interface{}) ResponseBody {
	return ResponseBody{
		Meta: Meta{
			Message:    message,
			StatusCode: statusCode,
		},
		Data: data,
	}
}

func FormatValidationError(err error) []string {
	var errors []string
	errors = append(errors, err.Error())

	return errors
}
