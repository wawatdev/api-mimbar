-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE reviews (
      id          serial NOT NULL PRIMARY KEY,
      schedule_id bigint NOT NULL,
      ustad_id    bigint NOT NULL,
      hunter_id   bigint NOT NULL,
      star        int NOT NULL,
      description text NOT NULL,
      show        int NOT NULL DEFAULT 1,
      created_at  TIMESTAMP NULL DEFAULT NULL,
      updated_at  TIMESTAMP NULL DEFAULT NULL
);
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE reviews;