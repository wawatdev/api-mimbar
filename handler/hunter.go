package handler

import (
	"api-mimbar/hunter"
	"github.com/gin-gonic/gin"
	"net/http"
)

type hunterHandler struct {
	service hunter.Service
}

func NewHunterHandler(hunterService hunter.Service) *hunterHandler {
	return &hunterHandler{hunterService}
}

func (h *hunterHandler) Create(c *gin.Context) {
	var input hunter.CreateHunterInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	hunterData, err := h.service.Create(input)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	c.JSON(http.StatusOK, hunterData)
	return
}
