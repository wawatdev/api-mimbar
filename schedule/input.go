package schedule

type CreateScheduleInput struct {
	UstadID []int `json:"ustad_id" binding:"required"`
	HunterID int
	Date string
	TimeStart string `json:"time_start" binding:"required"`
	TimeEnd string `json:"time_end" binding:"required"`
	Location string `json:"location" binding:"required"`
	Description string `json:"description" binding:"required"`
}

type CancelScheduleInput struct {
	ScheduleID int `json:"schedule_id" binding:"required"`
	Reason string `json:"reason" binding:"required"`
}

type GetScheduleInput struct {
	ID int `uri:"id" binding:"required"`
}

type ConfirmationScheduleInput struct {
	ScheduleID int `form:"schedule_id" binding:"required"`
}