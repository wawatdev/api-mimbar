package user

import "time"

type User struct {
	ID int
	Name string
	Email string
	Active bool
	Confirmation bool
	Status string
	Role string
	Phone string
	CreatedAt time.Time
	UpdatedAt time.Time
}
