package schedule

import (
	"api-mimbar/hunter"
	"api-mimbar/ustad"
	"time"
)

type CreateUpdateScheduleFormat struct {
	ID int `json:"id"`
	UstadID int `json:"ustad_id"`
	HunterID int `json:"hunter_id"`
	Date string `json:"date"`
	TimeStart string `json:"time_start"`
	TimeEnd string `json:"time_end"`
	Location string `json:"location"`
	Confirmation bool `json:"confirmation"`
	IsDone bool `json:"is_done"`
	IsCancel bool `json:"is_cancel"`
	Description string `json:"description"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func FormatCreateUpdateSchedule(schedule Schedule) CreateUpdateScheduleFormat {
	fSchedule := CreateUpdateScheduleFormat{
		ID:           schedule.ID,
		UstadID:      schedule.UstadID,
		HunterID:     schedule.HunterID,
		Date:         schedule.Date,
		TimeStart:    schedule.TimeStart,
		TimeEnd:      schedule.TimeEnd,
		Location:     schedule.Location,
		Confirmation: false,
		IsDone:       false,
		IsCancel:     false,
		Description:  schedule.Description,
		CreatedAt: schedule.CreatedAt,
		UpdatedAt: schedule.UpdatedAt,
	}
	if schedule.Confirmation == 1 {
		fSchedule.Confirmation = true
	}
	if schedule.IsCanceled == 1 {
		fSchedule.IsCancel = true
	}
	if schedule.IsDone == 1 {
		fSchedule.IsDone = true
	}
	return fSchedule
}

func FormatCreateUpdateSchedules(schedules []Schedule) []CreateUpdateScheduleFormat {
	fSchedules := []CreateUpdateScheduleFormat{}
	if len(schedules) > 0 {
		for _, schedule := range schedules {
			fSchedule := FormatCreateUpdateSchedule(schedule)
			fSchedules = append(fSchedules, fSchedule)
		}
	}
	return fSchedules
}

type ScheduleFormat struct {
	ID int `json:"id"`
	Date string `json:"date"`
	TimeStart string `json:"time_start"`
	TimeEnd string `json:"time_end"`
	Location string `json:"location"`
	Confirmation bool `json:"confirmation"`
	IsDone bool `json:"is_done"`
	IsCancel bool `json:"is_cancel"`
	Description string `json:"description"`
	CreatedAt time.Time `json:"created_at"`
}

func FormatSchedule(schedule Schedule) ScheduleFormat {
	fSchedule := ScheduleFormat{
		ID:           schedule.ID,
		Date:         schedule.Date,
		TimeStart:    schedule.TimeStart,
		TimeEnd:      schedule.TimeEnd,
		Location:     schedule.Location,
		Confirmation: false,
		IsDone:       false,
		IsCancel:     false,
		Description:  schedule.Description,
		CreatedAt:    schedule.CreatedAt,
	}
	if schedule.Confirmation == 1 {
		fSchedule.Confirmation = true
	}
	if schedule.IsDone == 1 {
		fSchedule.IsDone = true
	}
	if schedule.IsCanceled == 1 {
		fSchedule.IsCancel = true
	}

	return fSchedule
}

type FormatScheduleUstad struct {
	ScheduleFormat
	Hunter hunter.HunterFormat `json:"hunter"`
	Cancel interface{} `json:"cancel"`
}

func ScheduleUstadFormat(schedule Schedule) FormatScheduleUstad {
	fSchedule := FormatScheduleUstad{
		ScheduleFormat: FormatSchedule(schedule),
		Hunter:   hunter.HunterFormat{
			ID:           schedule.Hunter.ID,
			Name:         schedule.Hunter.Name,
			Email:        schedule.Hunter.Email,
			Active:       false,
			Confirmation: false,
			CreatedAt:    schedule.Hunter.CreatedAt,
		},
		Cancel: nil,
	}
	if schedule.Hunter.Active == 1 {
		fSchedule.Hunter.Active = true
	}
	if schedule.Hunter.Confirmation == 1 {
		fSchedule.Hunter.Confirmation = true
	}

	if schedule.CancelSchedule.ID != 0 {
		var Cancel = CancelScheduleData{
			ID:           schedule.CancelSchedule.ID,
			scheduleID:   schedule.CancelSchedule.ScheduleID,
			UstadCancel:  false,
			HunterCancel: false,
			Reason:       schedule.CancelSchedule.Reason,
			CreatedAt:    schedule.CancelSchedule.CreatedAt,
			UpdatedAt:    schedule.CancelSchedule.UpdatedAt,
		}

		if schedule.CancelSchedule.UstadCancel == 1 {
			Cancel.UstadCancel = true
		}
		if schedule.CancelSchedule.HunterCancel == 1 {
			Cancel.HunterCancel = true
		}
		fSchedule.Cancel = Cancel
	}

	return fSchedule
}

func SchedulesUstadFormat(schedules []Schedule) []FormatScheduleUstad {
	fSchedules := []FormatScheduleUstad{}
	if len(schedules) > 0 {
		for _, schedule := range schedules {
			fSchedule := ScheduleUstadFormat(schedule)
			fSchedules = append(fSchedules, fSchedule)
		}
	}
	return fSchedules
}

type FormatScheduleHunter struct {
	ScheduleFormat
	Ustad ustad.UstadFormat `json:"ustad"`
	Cancel interface{} `json:"cancel"`
}

func ScheduleHunterFormat(schedule Schedule) FormatScheduleHunter {
	fSchedule := FormatScheduleHunter{
		ScheduleFormat: FormatSchedule(schedule),
		Ustad:    ustad.UstadFormat{
			ID:           schedule.Ustad.ID,
			Name:         schedule.Ustad.Name,
			Email:        schedule.Ustad.Email,
			Active:       false,
			Confirmation: false,
			Status:       schedule.Ustad.Status,
			CreatedAt:    schedule.Ustad.CreatedAt,
		},
		Cancel: nil,
	}
	if schedule.Ustad.Active == 1 {
		fSchedule.Ustad.Active = true
	}
	if schedule.Ustad.Confirmation == 1 {
		fSchedule.Ustad.Confirmation = false
	}

	if schedule.CancelSchedule.ID != 0 {
		var Cancel = CancelScheduleData{
			ID:           schedule.CancelSchedule.ID,
			scheduleID:   schedule.CancelSchedule.ScheduleID,
			UstadCancel:  false,
			HunterCancel: false,
			Reason:       schedule.CancelSchedule.Reason,
			CreatedAt:    schedule.CancelSchedule.CreatedAt,
			UpdatedAt:    schedule.CancelSchedule.UpdatedAt,
		}

		if schedule.CancelSchedule.UstadCancel == 1 {
			Cancel.UstadCancel = true
		}
		if schedule.CancelSchedule.HunterCancel == 1 {
			Cancel.HunterCancel = true
		}
		fSchedule.Cancel = Cancel
	}
	return fSchedule
}

func SchedulesHunterFormat(schedules []Schedule) []FormatScheduleHunter {
	fSchedules := []FormatScheduleHunter{}
	if len(schedules) > 0 {
		for _, schedule := range schedules {
			fSchedule := ScheduleHunterFormat(schedule)
			fSchedules = append(fSchedules, fSchedule)
		}
	}
	return fSchedules
}

type CancelScheduleData struct {
	ID int `json:"id"`
	scheduleID int `json:"schedule_id"`
	UstadCancel bool `json:"ustad_cancel"`
	HunterCancel bool `json:"hunter_cancel"`
	Reason string `json:"reason"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type CancelScheduleFormat struct {
	ScheduleFormat
	Cancel CancelScheduleData `json:"cancel"`
}

func FormatCancelSchedule(schedule Schedule) CancelScheduleFormat {
	fSchedule := CancelScheduleFormat{
		ScheduleFormat: FormatSchedule(schedule),
		Cancel:         CancelScheduleData{
			ID:           schedule.CancelSchedule.ID,
			scheduleID:   schedule.CancelSchedule.ScheduleID,
			UstadCancel:  false,
			HunterCancel: false,
			Reason:       schedule.CancelSchedule.Reason,
			CreatedAt:    schedule.CancelSchedule.CreatedAt,
			UpdatedAt:    schedule.CancelSchedule.UpdatedAt,
		},
	}
	if schedule.CancelSchedule.UstadCancel == 1 {
		fSchedule.Cancel.UstadCancel = true
	}
	if schedule.CancelSchedule.HunterCancel == 1 {
		fSchedule.Cancel.HunterCancel = true
	}
	return fSchedule
}