package hunter

type CreateHunterInput struct {
	Name string `json:"name"`
	Email string `json:"email"`
	Password string `json:"password"`
}

type UpdateHunterInput struct {
	ID int
	Name string `json:"name" binding:"required"`
	Email string `json:"email"`
	Password string `json:"password"`
	Phone string `json:"phone"`
}
