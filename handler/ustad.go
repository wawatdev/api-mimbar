package handler

import (
	"api-mimbar/helper"
	"api-mimbar/user"
	"api-mimbar/ustad"
	"github.com/gin-gonic/gin"
	"net/http"
)

type ustadHandler struct {
	ustadService ustad.Service
}

func NewUstadHandler(ustadService ustad.Service) *ustadHandler {
	return &ustadHandler{ustadService}
}

func (h *ustadHandler) Create(c *gin.Context) {
	var input ustad.CreateUstadInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	ustadData, err := h.ustadService.Create(input)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	c.JSON(http.StatusOK, ustadData)
	return
}

func (h *ustadHandler) GetAllWithActiveAndConfirm(c *gin.Context) {
	ustads, err := h.ustadService.GetAllWithActiveConfirmAndSchedule()
	if err != nil {
		response := helper.ApiResponse("Error get ustad", http.StatusUnprocessableEntity, err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("List of ustads", http.StatusOK, ustad.FormatUstads(ustads))
	c.JSON(http.StatusOK, response)
	return
}

func (h *ustadHandler) CreateSchedule(c *gin.Context) {
	var input ustad.CreateScheduleUstadInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed create schedule", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	userData := c.MustGet("currentUser").(user.User)

	input.UstadID = userData.ID

	schedules, err := h.ustadService.CreateSchedule(input)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed create schedule", http.StatusUnprocessableEntity, errMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	response := helper.ApiResponse("Successfully create schedule", http.StatusOK, ustad.FormatSchedulesUstad(schedules))
	c.JSON(http.StatusOK, response)
	return
}

func (h *ustadHandler) UpdateSchedule(c *gin.Context) {
	var input ustad.CreateScheduleUstadInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed update schedule ustad", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	userData := c.MustGet("currentUser").(user.User)

	input.UstadID = userData.ID

	schedulesUstad, err := h.ustadService.UpdateSchedule(input)
	if err != nil {
		errMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("failed update schedule ustad", http.StatusBadRequest, errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Update Schedule Successfully", http.StatusOK, ustad.FormatSchedulesUstad(schedulesUstad))
	c.JSON(http.StatusOK, response)
	return
}
