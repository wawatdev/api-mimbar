package ustad

import (
	"gorm.io/gorm"
)

type Repository interface {
	SaveOrUpdate(ustad Ustad) (Ustad, error)
	FindByEmail(email string) (Ustad, error)
	FindByID(ID int) (Ustad, error)
	FindAllWithActiveConfirmAndSchedule() ([]Ustad, error)
	SaveScheduleBatch(schedules []ScheduleUstad) ([]ScheduleUstad, error)
	FindScheduleByUstadID(ustadID int) ([]ScheduleUstad, error)
	DeleteScheduleByUstadID(ustadID int) (bool, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) SaveOrUpdate(ustad Ustad) (Ustad, error){
	err := r.db.Save(&ustad).Error
	if err != nil {
		return ustad, err
	}
	return ustad, nil
}

func (r *repository) FindByEmail(email string) (Ustad, error){
	var ustad Ustad
	err := r.db.Where("email = ?", email).Find(&ustad).Error
	if err != nil {
		return ustad, err
	}
	return ustad, nil
}

func (r *repository) FindByID(ID int) (Ustad, error){
	var ustad Ustad
	err := r.db.Where("id = ?", ID).Find(&ustad).Error
	if err != nil {
		return ustad, err
	}
	return ustad, nil
}

func (r *repository) FindAllWithActiveConfirmAndSchedule() ([]Ustad, error) {
	ustads := []Ustad{}
	err := r.db.Preload("ScheduleUstad.Day").Where("active = 1 and confirmation = 1").Find(&ustads).Error
	if err != nil {
		return ustads, err
	}
	return ustads, nil
}

func (r *repository) SaveScheduleBatch(schedules []ScheduleUstad) ([]ScheduleUstad, error) {
	err := r.db.Save(&schedules).Error
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (r *repository) FindScheduleByUstadID(ustadID int) ([]ScheduleUstad, error){
	var schedulesUstad []ScheduleUstad
	err := r.db.Where("ustad_id = ?", ustadID).Find(&schedulesUstad).Error
	if err != nil {
		return schedulesUstad, err
	}
	return schedulesUstad, nil
}

func (r *repository) DeleteScheduleByUstadID(ustadID int) (bool, error) {
	var scheduleUstad ScheduleUstad
	err := r.db.Where("ustad_id = ?", ustadID).Delete(&scheduleUstad).Error
	if err != nil {
		return false, err
	}
	return true, nil
}