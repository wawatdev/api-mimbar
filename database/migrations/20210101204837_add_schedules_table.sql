-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE schedules (
     id          serial NOT NULL PRIMARY KEY,
     ustad_id    bigint NOT NULL,
     hunter_id   bigint NOT NULL,
     date        DATE NOT NULL,
     time_start  TIME NOT NULL,
     time_end   TIME NOT NULL,
     location    varchar(255) NOT NULL,
     confirmation int NOT NULL,
     is_done     int NOT NULL,
     is_canceled int NOT NULL,
     description text NOT NULL,
     created_at  TIMESTAMP NULL DEFAULT NULL,
     updated_at  TIMESTAMP NULL DEFAULT NULL
);
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE schedules;