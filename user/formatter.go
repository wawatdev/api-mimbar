package user

import "time"

type UserFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Email string `json:"email"`
	Active bool `json:"active"`
	Phone string `json:"phone"`
	Confirmation bool `json:"confirmation"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type UserToken struct {
	UserFormat
	Token interface{} `json:"token"`
}

func FormatLogin(user User, token string) UserToken {
	format := UserToken{
		UserFormat: FormatUser(user),
	}
	if token != "" {
		format.Token = token
	}
	return format
}

func FormatUser(user User) UserFormat {
	return UserFormat{
		ID:           user.ID,
		Name:         user.Name,
		Email:        user.Email,
		Active:       user.Active,
		Confirmation: user.Confirmation,
		Phone: user.Phone,
		CreatedAt:    user.CreatedAt,
		UpdatedAt:    user.UpdatedAt,
	}
}

