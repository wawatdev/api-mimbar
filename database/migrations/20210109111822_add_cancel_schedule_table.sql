-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE cancel_schedules (
   id          serial NOT NULL PRIMARY KEY,
   schedule_id    bigint NOT NULL,
   ustad_cancel   int NOT NULL,
   hunter_cancel  int NOT NULL,
   reason  text NOT NULL,
   created_at  TIMESTAMP NULL DEFAULT NULL,
   updated_at  TIMESTAMP NULL DEFAULT NULL
);
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE cancel_schedules;