package schedule

import (
	"api-mimbar/user"
	"api-mimbar/ustad"
	"errors"
	"time"
)

type Service interface {
	Create(input CreateScheduleInput) ([]Schedule, error)
	GetAllByUstadID(ustadID int) ([]Schedule, error)
	GetAllByHunterID(hunterID int) ([]Schedule, error)
	CancelSchedule(input CancelScheduleInput, role string) (Schedule, error)
	Confirmation(scheduleID int, user user.User) (Schedule, error)
	GetByIDWithRole(ScheduleID int, role string) (Schedule, error)
	Done(ScheduleId int, user user.User) (Schedule, error)
	GetByID(scheduleID int) (Schedule, error)
}

type service struct {
	repository Repository
	ustadService ustad.Service
}

func NewService(repository Repository, ustadService ustad.Service) *service {
	return &service{repository, ustadService}
}

func (s *service) Create(input CreateScheduleInput) ([]Schedule, error){
	schedules := []Schedule{}
	if len(input.UstadID) < 1 {
		return schedules, errors.New("ustad id not found")
	}

	dateSchedule, err := time.Parse("2006-01-02", input.Date)
	if err != nil {
		return schedules, err
	}

	today, err := time.Parse("2006-01-02", time.Now().Format("2006-01-02"))
	if err != nil {
		return schedules, err
	}
	if !today.Equal(dateSchedule) {
		if time.Now().After(dateSchedule) {
			return schedules, errors.New("date cannot before today")
		}
	}

	for _, ustad_id := range input.UstadID {
		//check ustad exist or not
		_ ,err := s.ustadService.GetByIDActiveAndConfirmed(ustad_id)
		if err != nil {
			return schedules, err
		}

		schedule := Schedule{
			UstadID:      ustad_id,
			HunterID:     input.HunterID,
			Date:         input.Date,
			TimeStart:    input.TimeStart,
			TimeEnd:      input.TimeEnd,
			Location:     input.Location,
			Description:  input.Description,
			CreatedAt:    time.Now(),
			UpdatedAt:    time.Now(),
		}

		checkExist, err := s.repository.CheckExist(schedule)
		if err != nil {
			return schedules, err
		}

		if checkExist {
			return schedules, errors.New("schedule has been add")
		}
		schedules = append(schedules, schedule)
	}

	if len(schedules) > 0 {
		schedules, err := s.repository.SaveBatch(schedules)
		if err != nil {
			return schedules, err
		}
	}
	return schedules, nil
}

func (s *service) GetAllByUstadID(ustadID int) ([]Schedule, error) {
	schedules, err := s.repository.FindByUstadID(ustadID)
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (s *service) GetAllByHunterID(hunterID int) ([]Schedule, error) {
	schedules, err := s.repository.FindByHunterID(hunterID)
	if err != nil {
		return schedules, err
	}
	return schedules, nil
}

func (s *service) CancelSchedule(input CancelScheduleInput, role string) (Schedule, error) {
	scheduleData, err := s.repository.FindByID(input.ScheduleID)
	if err != nil {
		return scheduleData, err
	}
	if scheduleData.ID == 0 {
		return scheduleData, errors.New("schedule not found")
	}

	if scheduleData.IsCanceled == 1 {
		return scheduleData, errors.New("schedule has been cancel before")
	}

	scheduleData.IsCanceled = 1
	scheduleData.CancelSchedule = CancelSchedule{
		ScheduleID:   scheduleData.ID,
		UstadCancel:  0,
		HunterCancel: 0,
		Reason:       input.Reason,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	if role == "ustad" {
		scheduleData.CancelSchedule.UstadCancel = 1
	}else if role == "hunter" {
		scheduleData.CancelSchedule.HunterCancel = 1
	}else{
		return scheduleData, errors.New("user not authorized")
	}

	newSchedule, err := s.repository.Cancel(scheduleData)
	if err != nil {
		return newSchedule, err
	}
	return newSchedule, nil
}

func (s *service) Confirmation(scheduleID int, user user.User) (Schedule, error) {
	scheduleData, err := s.repository.FindByID(scheduleID)
	if err != nil {
		return scheduleData, err
	}
	if scheduleData.ID == 0 {
		return scheduleData, errors.New("schedule not found")
	}
	if scheduleData.UstadID != user.ID {
		return scheduleData, errors.New("you don't have permission with this schedule")
	}
	if scheduleData.IsCanceled == 1 {
		return scheduleData, errors.New("schedule has been cancled before")
	}
	if scheduleData.IsDone == 1 {
		return scheduleData, errors.New("schedule has been done before")
	}
	if scheduleData.Confirmation == 1 {
		return scheduleData, errors.New("schedule has been confirm before")
	}

	scheduleData.Confirmation = 1;

	scheduleData, err = s.repository.SaveOrUpdate(scheduleData)
	if err != nil {
		return scheduleData, err
	}
	return scheduleData, nil
}

func (s *service) GetByIDWithRole(ScheduleID int, role string) (Schedule, error) {
	var schedule Schedule
	if role == "ustad" {
		schedule, err := s.repository.FindByIDWithHunter(ScheduleID)
		if err != nil {
			return schedule, err
		}
		return schedule, nil
	}else if role == "hunter" {
		schedule, err := s.repository.FindByIDWithUstad(ScheduleID)
		if err != nil {
			return schedule, err
		}
		return schedule, nil
	}
	return schedule, errors.New("failed process data")
}

func (s *service) Done(ScheduleId int, user user.User) (Schedule, error) {
	scheduleData, err := s.repository.FindByID(ScheduleId)
	if err != nil {
		return scheduleData, err
	}
	if scheduleData.ID == 0 {
		return scheduleData, errors.New("schedule not found")
	}
	if user.Role == "ustad" {
		if scheduleData.UstadID != user.ID {
			return scheduleData, errors.New("you don't have permission with this schedule")
		}
	}else if user.Role == "hunter" {
		if scheduleData.HunterID != user.ID {
			return scheduleData, errors.New("you don't have permission with this schedule")
		}
	}
	if scheduleData.IsCanceled == 1 {
		return scheduleData, errors.New("schedule has been cancled before")
	}
	if scheduleData.IsDone == 1 {
		return scheduleData, errors.New("schedule has been done before")
	}
	if scheduleData.Confirmation != 1 {
		return scheduleData, errors.New("schedule need confirm before")
	}

	//update schedule to status done
	scheduleData.IsDone = 1
	scheduleData, err = s.repository.SaveOrUpdate(scheduleData)
	if err != nil {
		return scheduleData, err
	}
	return scheduleData, nil
}

func (s *service) GetByID(scheduleID int) (Schedule, error) {
	scheduleData, err := s.repository.FindByID(scheduleID)
	if err != nil {
		return scheduleData, err
	}
	if scheduleData.ID == 0 {
		return scheduleData, errors.New("schedule not found")
	}

	return scheduleData, nil
}

