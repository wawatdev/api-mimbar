package schedule

import (
	"api-mimbar/hunter"
	"api-mimbar/ustad"
	"time"
)

type Schedule struct {
	ID int
	UstadID int
	HunterID int
	Date string
	TimeStart string
	TimeEnd string
	Location string
	Confirmation int8
	IsDone int8
	IsCanceled int8
	Description string
	CreatedAt time.Time
	UpdatedAt time.Time
	Ustad ustad.Ustad
	Hunter hunter.Hunter
	CancelSchedule CancelSchedule
}

type CancelSchedule struct {
	ID int
	ScheduleID int
	UstadCancel int
	HunterCancel int
	Reason string
	CreatedAt time.Time
	UpdatedAt time.Time
}